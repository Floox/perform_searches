import time
from internal_utils import start_search  # returns search uuid
from internal_utils import check_search_status  # returns current search status as str
from internal_utils import get_search_results  # returns search results
from asyncio import sleep, Queue, get_event_loop, create_task, run
from concurrent.futures import ThreadPoolExecutor


def sync_perform_searches(params_list):
    results = []
    for params in params_list:
        s = start_search(params)
        while check_search_status(s) != 'completed':
            time.sleep(1)
        results.append(get_search_results(s))
    return results


async def is_status_completed(s):
    ll = list()
    while check_search_status(s, ll) != 'completed':
        await sleep(1)


async def search_algorithm(q: Queue, res_list: list, thread_pool_size=5):
    while 1:
        param = await q.get()
        with ThreadPoolExecutor(thread_pool_size) as pool:
            s = await get_event_loop().run_in_executor(pool, start_search, param)
            await is_status_completed(param)
            result = await get_event_loop().run_in_executor(pool, get_search_results, s)
        res_list.append(result)
        q.task_done()


async def perform_searches(params_list):
    pp = Queue()
    for param in params_list:
        pp.put_nowait(param)
    workers = list()
    results = list()
    for i in range(5):
        task = create_task(search_algorithm(pp, results))
        workers.append(task)
    await pp.join()
    for w in workers:
        w.cancel()
    return results



test_data = list('asasdga')

print('SYNC:')
start = time.time()
sync_perform_searches(test_data)
print(int(time.time() - start))

print('ASYNC:')
start = time.time()
r = run(perform_searches(test_data))
print(time.time() - start)
